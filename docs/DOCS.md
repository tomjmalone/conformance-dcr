# `DOCS`

See [./QUICK-START](./QUICK-START.md).

## Dynamic Client Registration

### Developer Zone

* Dynamic Client Registration - v3.2: <https://openbanking.atlassian.net/wiki/spaces/DZ/pages/1078034771/Dynamic+Client+Registration+-+v3.2>.
* Dynamic Client Registration - v3.1: <https://openbanking.atlassian.net/wiki/spaces/DZ/pages/937066600/Dynamic+Client+Registration+-+v3.1>.

### GitHub Pages

**NB**: Links are subject to change, so please do not bookmark them for now.

* Dynamic Client Registration v3.2: <https://openbankinguk.github.io/dcr-docs-pub/v3.2/dynamic-client-registration.html>.
* Dynamic Client Registration v3.1: <https://openbankinguk.github.io/dcr-docs-pub/v3.1/dynamic-client-registration.html>.
